return require("packer").startup(function(use)

  -- Packer can manage itself
  use("wbthomason/packer.nvim")

  use({
    "lewis6991/impatient.nvim",
    config = function()
      require("impatient")
    end,
  })

  -- Theme
  use({
    "EdenEast/nightfox.nvim",
    run = ":source ~/.config/nvim/lua/config/theme.lua",
  })

  -- File tree
  use({
    "nvim-tree/nvim-tree.lua",
    requires = {
      "nvim-tree/nvim-web-devicons",
    },
    tag = "nightly", -- optional, updated every week
  })

  -- Top bar
  use({
    "romgrk/barbar.nvim",
    requires = { "nvim-tree/nvim-web-devicons" },
  })
  use({
    "nvim-telescope/telescope.nvim",
    requires = { { "nvim-lua/popup.nvim" }, { "nvim-lua/plenary.nvim" } },
  })
  use({
    "nvim-telescope/telescope-smart-history.nvim",
    requires = { "tami5/sqlite.lua" },
  })
  use("natecraddock/telescope-zf-native.nvim")

  use({
    "lewis6991/gitsigns.nvim",
  })
  use("gpanders/editorconfig.nvim")

  use({
    "nvim-treesitter/nvim-treesitter",
    run = function()
      require("nvim-treesitter.install").update({ with_sync = true })
    end,
  })
  use("nvim-treesitter/playground")
  use("nvim-treesitter/nvim-treesitter-context")
  use("nvim-treesitter/nvim-treesitter-textobjects")

  -- LSP
  use("neovim/nvim-lspconfig") -- Configurations for Nvim LSP

  use({
    "numToStr/Comment.nvim",
    config = function()
      require("Comment").setup()
    end,
  })

  use({
    "nvim-lualine/lualine.nvim",
    requires = { "nvim-tree/nvim-web-devicons", opt = true },
  })
  use({
    "rcarriga/nvim-notify",
    config = function()
      vim.notify = require("notify")
    end,
  })

  -- LARK syntax highlighting
  use({ "lark-parser/vim-lark-syntax", ft = { "lark" } })

  -- Completion
  use({
    "hrsh7th/nvim-cmp",
    requires = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-nvim-lua",
      "hrsh7th/cmp-nvim-lsp-signature-help",
      "hrsh7th/cmp-path",
      "onsails/lspkind.nvim",
      "L3MON4D3/LuaSnip",
      "saadparwaiz1/cmp_luasnip",
      "lukas-reineke/cmp-under-comparator",
    },
  })
    use({ "tzachar/cmp-tabnine", run = "./install.sh", requires = "hrsh7th/nvim-cmp" })

  use({
    "jose-elias-alvarez/null-ls.nvim",
    requires = { "nvim-lua/plenary.nvim" },
  })

  use 'mfussenegger/nvim-lint'

  use({
    "kylechui/nvim-surround",
    config = function()
      require("nvim-surround").setup({})
    end,
  })

  use("williamboman/mason.nvim")
  use("williamboman/mason-lspconfig.nvim")

  use({
    "simrat39/symbols-outline.nvim",
    config = function()
      require("symbols-outline").setup()
    end,
  })

  use({
    "kosayoda/nvim-lightbulb",
    config = function()
      require("nvim-lightbulb").setup({
        sign = {
          enabled = false,
        },
        virtual_text = {
          enabled = true,
        },
        autocmd = { enabled = true },
      })
    end,
  })

  use({
    "stevearc/dressing.nvim",
    config = function()
      require("dressing").setup({
        input = {
          get_config = function(opts)
            if opts.completion == "file" then
              return {
                -- enabled = false,
                relative = "editor",
              }
            end
            if opts.prompt:find("^Trash ") ~= nil then
              return {
                relative = "editor",
              }
            end
          end,
        },
        select = {
          backend = { "telescope", "builtin" },
          get_config = function(opts)
            if opts.kind == "codeaction" then
              return {
                telescope = require("telescope.themes").get_cursor(),
              }
            end
          end,
        },
      })
    end,
  })

  use({
    "linty-org/readline.nvim",
    config = function()
      local readline = require("readline")
      vim.keymap.set("!", "<M-f>", readline.forward_word)
      vim.keymap.set("!", "<M-b>", readline.backward_word)
      vim.keymap.set("!", "<C-a>", readline.beginning_of_line)
      vim.keymap.set("!", "<C-e>", readline.end_of_line)
      -- vim.keymap.set('!', '<C-w>', readline.backward_kill_word)
      vim.keymap.set("!", "<C-k>", readline.kill_line)
    end,
  })

  use({
    "catppuccin/nvim",
    as = "catppuccin",
    config = function()
      vim.g.catppuccin_flavour = "macchiato" -- latte, frappe, macchiato, mocha
      require("catppuccin").setup()
      -- vim.api.nvim_command "colorscheme catppuccin"
    end,
  })

  use({
    "rmagatti/auto-session",
    config = function()
      require("auto-session").setup({
        log_level = "error",
        auto_session_use_git_branch = true,
      })
    end,
  })

  use({
    "folke/neodev.nvim",
    config = function()
      require("neodev").setup({})
    end,
  })
end)
