vim.g.mapleader = " "

vim.keymap.set({ "n", "v" }, "<Space>", "<Nop>")
vim.keymap.set({"i", "n", "v", "c"}, "<C-q>", "<cmd>confirm quitall<CR>", { silent = true })
vim.keymap.set({ "n", "i" }, "<C-s>", "<cmd>update<CR>", { silent = true })
vim.keymap.set("n", "<Leader>p", "<cmd>set wrap!<CR>", { silent = true })

-- Switch ; and :
vim.keymap.set({ "n", "v" }, ";", ":")
vim.keymap.set({ "n", "v" }, ":", ";")

-- Switch Visual and Visual Block modes
vim.keymap.set("n", "v", "<C-v>")
vim.keymap.set("n", "<C-v>", "v")

vim.keymap.set("n", "<C-w>", "<cmd>bdelete<CR>", { silent = true })
vim.keymap.set("n", "<Leader>w", "<cmd>%bdelete<CR>", { silent = true })

-- Insert mode movement
vim.keymap.set("i", "<C-h>", "<Left>")
vim.keymap.set("i", "<C-j>", "<Down>")
vim.keymap.set("i", "<C-k>", "<Up>")
vim.keymap.set("i", "<C-l>", "<Right>")

-- Visual linewise up and down by default, also center the screen
vim.keymap.set("n", "j", "gj")
vim.keymap.set("n", "k", "gk")
vim.keymap.set({ "n", "v" }, "J", "10gjzz")
vim.keymap.set({ "n", "v" }, "K", "10gkzz")

-- Remap old K to H
vim.keymap.set({ "n", "v" }, "H", "K")

-- Splits
vim.keymap.set("n", "<M-h>", "<C-W><left>")
vim.keymap.set("n", "<M-l>", "<C-W><right>")
vim.keymap.set("n", "<M-k>", "<C-W><up>")
vim.keymap.set("n", "<M-j>", "<C-W><down>")
vim.keymap.set("i", "<M-h>", "<C-o><C-W><left>")
vim.keymap.set("i", "<M-l>", "<C-o><C-W><right>")
vim.keymap.set("i", "<M-k>", "<C-o><C-W><up>")
vim.keymap.set("i", "<M-j>", "<C-o><C-W><down>")
vim.keymap.set("n", "<C-c>", "<C-W><C-W>") -- cycle windows

-- Stay in vusial mode when indenting
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

-- When jump to next match also center screen
vim.keymap.set("n", "n", "nzz")
vim.keymap.set("n", "N", "Nzz")

-- Quick replay 'q' macro
-- flow: qq for starting macro on q, Q for replaying it
vim.keymap.set("n", "Q", "@q")

-- Resizing
vim.keymap.set({ "n", "v" }, "<C-left>", "<C-W>5<")
vim.keymap.set({ "n", "v" }, "<C-right>", "<C-W>5>")
vim.keymap.set({ "n", "v" }, "<C-up>", "<C-W>5+")
vim.keymap.set({ "n", "v" }, "<C-down>", "<C-W>5-")

-- Changing text
-- c into c register
vim.keymap.set("n", "c", '"cc')
vim.keymap.set("n", "C", '"cC')
vim.keymap.set("n", "cC", "<cmd>%d c<CR>i")
vim.keymap.set("n", "dD", "<cmd>%d<CR>")
vim.keymap.set("n", "yY", "<cmd>%y<CR>")
vim.keymap.set("n", "vV", "ggVG")

-- Replace the rest of the line with the contents of clipboard
vim.keymap.set("n", "R", '"cDp')

-- Poor man's autopairs
for _, q in ipairs({ '"', "'", "`" }) do
	vim.keymap.set("i", q .. q, q .. q .. "<left>")
end
for _, p in ipairs({ "()", "[]", "<>", "{}" }) do
	local l = p:sub(1, 1)
	local r = p:sub(2, 2)
	vim.keymap.set("i", string.format("%s%s", l, l), string.format("%s%s<left>", l, r))
	vim.keymap.set("i", string.format("%s<CR>", l), string.format("%s<CR><CR>%s<up><C-f>", l, r))
end

vim.keymap.set({ "n", "v" }, "<Esc>", "<cmd>nohl<CR><Esc>", { silent = true })

vim.keymap.set("n", "s", "i<CR><Esc>")
vim.keymap.set("n", "S", "J")
