local opts = { silent = true }
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, opts)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)

--    
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

vim.diagnostic.config({
  virtual_text = false,
  severity_sort = true,
  float = {
    source = "always",
    scope = "line",
    border = "rounded",
    focusable = false,
  },
  virtual_lines = false,
})

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" })

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(_, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap = true, silent = true, buffer = bufnr }
  vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
  vim.keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", { silent = true })
  vim.keymap.set("n", "gt", "<cmd>Telescope lsp_type_definitions<CR>", { silent = true })
  vim.keymap.set("n", "gi", "<cmd>Telescope lsp_implementations<CR>", { silent = true })
  vim.keymap.set("n", "gr", "<cmd>Telescope lsp_references<CR>", { silent = true })
  vim.keymap.set("n", "H", vim.lsp.buf.hover, bufopts)
  vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set("n", "<leader>r", vim.lsp.buf.rename, bufopts)
  vim.keymap.set("n", "<leader>c", vim.lsp.buf.code_action, bufopts)
  vim.keymap.set("n", "<leader>l", function()
    vim.lsp.buf.format({ async = true })
  end, bufopts)
  --[[ vim.api.nvim_create_autocmd("CursorHold", {
    buffer = bufnr,
    callback = function()
      vim.diagnostic.open_float({
        scope = "line",
        focusable = false,
        -- close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
        border = "rounded",
      })
    end,
    group = vim.api.nvim_create_augroup("OpenFloat", {clear = true}),
  }) ]]
end

-- Setup completion capabilities
local capabilities = require("cmp_nvim_lsp").default_capabilities()

local lspconfig = require("lspconfig")
lspconfig.pyright.setup({
  capabilities = capabilities,
  settings = {
    pyright = {
      disableOrganizeImports = true,
    },
    python = {
      analysis = {
        autoImportCompletions = true,
        autoSearchPaths = true,
        diagnosticMode = "openFilesOnly",
        useLibraryCodeForTypes = true,
        typeCheckingMode = "basic",
      },
    },
  },
  on_attach = on_attach,
  on_init = function(client)
    local path = client.workspace_folders[1].name
    if string.find(path, "quantlane") then
      local quantlane_settings = {
        python = {
          analysis = {
            diagnosticSeverityOverrides = {
              reportPrivateImportUsage = "none",
              reportGeneralTypeIssues = "none",
              reportMissingTypeStubs = "none",
            },
          },
        },
      }
      client.config.settings = vim.tbl_deep_extend("error", client.config.settings, quantlane_settings)
    end
    if string.find(path, "/nano/") then
      local nano_settings = {
        python = {
          analysis = {
            extraPaths = { "src/" },
          },
        },
      }
      client.config.settings = vim.tbl_deep_extend("error", client.config.settings, nano_settings)
    end
    vim.g["lsp_settings_pyright"] = client.config.settings
    return true
  end,
})

lspconfig.bashls.setup({
  capabilities = capabilities,
  on_attach = on_attach,
  on_init = function(client)
    vim.g["lsp_settings_bashls"] = client.config.settings
    return true
  end,
})

lspconfig.tailwindcss.setup({
  capabilities = capabilities,
  on_attach = on_attach,
  on_init = function(client)
    vim.g["lsp_settings_tailwindcss"] = client.config.settings
    return true
  end,
})

lspconfig.tsserver.setup({
  capabilities = capabilities,
  on_attach = on_attach,
  on_init = function(client)
    vim.g["lsp_settings_tsserver"] = client.config.settings
    return true
  end,
})

lspconfig.astro.setup({
  capabilities = capabilities,
  on_attach = on_attach,
  on_init = function(client)
    vim.g["lsp_settings_astro"] = client.config.settings
    return true
  end,
})

lspconfig.sumneko_lua.setup({
  capabilities = capabilities,
  settings = {
    Lua = {
      completion = {
        callSnippet = "Replace",
      },
      telemetry = {
        enable = false,
      },
    },
  },
  on_attach = on_attach,
})
