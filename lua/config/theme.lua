local nightfox = require("nightfox")
local groups = {
  all = {
    TreesitterContext = { bg = "bg3", style = "bold" },
    TreesitterContextLineNumber = { bg = "bg3", fg = "fg1", style = "bold" },
    VertSplit = { fg = "palette.blue.dim" },
    BufferCurrent = { bg = "sel1", fg = "fg1" },
    BufferCurrentIndex = { bg = "sel1", fg = "diag.info" },
    BufferCurrentMod = { bg = "sel1", fg = "diag.warn" },
    BufferCurrentSign = { bg = "sel1", fg = "diag.info" },
    BufferCurrentTarget = { bg = "sel1", fg = "diag.error" },
    NormalFloat = { bg = "NONE" },
    DiagnosticVirtualTextError = { style = "bold" },
    DiagnosticVirtualTextWarn = { style = "bold" },
    DiagnosticVirtualTextInfo = { style = "bold" },
    DiagnosticVirtualTextHint = { style = "bold" },
  },
}
nightfox.setup({ options = { styles = { comments = "italic" } }, groups = groups })
nightfox.compile()
