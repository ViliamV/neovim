vim.opt.number = true
vim.opt.wrap = false
vim.opt.showmode = false
vim.opt.linebreak = true
vim.opt.showcmd = true
vim.opt.mouse = "a"
vim.opt.clipboard = "unnamedplus"
vim.opt.hidden = true
vim.opt.laststatus = 3 -- global statusline for all windows
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.swapfile = false
vim.opt.undofile = true

-- Neovide settings
vim.cmd([[set guifont=VictorMono\ Nerd\ Font,Apple\ Color\ Emoji:h14]])
-- let g:neovide_refresh_rate=60
vim.cmd([[let g:neovide_cursor_animation_length=0.0]])
-- vmap <C-C> "+y
-- nmap <C-V> "+p
-- inoremap <C-V> <C-r>+
-- cnoremap <C-V> <C-r>+
-- if exists("g:neovide")
--   set guifont=VictorMono\ Nerd\ Font:h14
--   " neovide settings
--   let g:neovide_refresh_rate=120
--   let g:neovide_curson_animation_length=0.05
--   let g:neovide_floating_blur_amount_x = 2.0
--   let g:neovide_floating_blur_amount_y = 2.0
--   let g:neovide_cursor_vfx_mode = ""
--   vmap <C-C> "+y
--   nmap <C-V> "+p
--   inoremap <C-V> <C-r>+
--   cnoremap <C-V> <C-r>+
-- endif

--
vim.opt.list = true
vim.opt.listchars = { tab = "▸ ", extends = "❯", precedes = "❮", trail = "·", nbsp = "·", eol = "¬" }
vim.opt.showbreak = "↪"

vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

vim.opt.foldmethod = "marker"

vim.opt.scrolloff = 5
vim.opt.sidescrolloff = 20

-- Keep long lines from slowing Vim too much
-- vim.opt.synmaxcol = 300
--
vim.opt.completeopt = { "menu", "menuone", "noselect" }
vim.opt.shortmess:append("c") -- don't show don't give "match 1 of 2" messages

vim.g.do_filetype_lua = 1
vim.g.did_load_filetypes = 1

vim.opt.fillchars = { -- thicker borders
  horiz = "━",
  horizup = "┻",
  horizdown = "┳",
  vert = "┃",
  vertleft = "┫",
  vertright = "┣",
  verthoriz = "╋",
}

vim.o.sessionoptions = "blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal"
-- vim.o.splitkeep = "screen"

vim.opt.updatetime = 500

vim.opt_local.formatoptions:remove({"o", "r", "2", "t", "a"})
-- vim.opt_local.formatoptions:prepend({"c", "q", "n", "j"})
