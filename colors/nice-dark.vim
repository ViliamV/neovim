" Nice dark - fork of:
" noctu.vim - Vim color scheme for 16-color terminals

" Scheme setup {{{
set background=dark
hi! clear

if exists('syntax_on')
  syntax reset
endif

let g:colors_name='nice-dark'

"}}}
" Vim UI {{{
hi Cursor              ctermfg=1     ctermbg=2
hi CursorLine          ctermbg=0     cterm=NONE
hi MatchParen          ctermfg=3     ctermbg=NONE  cterm=bold,underline
hi Pmenu               ctermfg=15    ctermbg=0
hi PmenuThumb          ctermbg=7
hi PmenuSBar           ctermbg=8
hi PmenuSel            ctermfg=0     ctermbg=4
hi ColorColumn         ctermbg=0
hi SpellBad            ctermfg=15 ctermbg=1
hi! link SpellCap SpellBad
hi! link SpellRare SpellBad
hi! link SpellLocal SpellBad
hi NonText             ctermfg=8
hi LineNr              ctermfg=8     ctermbg=NONE
hi CursorLineNr        ctermfg=6    ctermbg=NONE cterm=bold
hi Visual              ctermfg=16     ctermbg=12
hi IncSearch           ctermfg=3     ctermbg=NONE cterm=reverse,bold,italic
hi Search              ctermfg=11     ctermbg=NONE cterm=reverse
hi StatusLine          ctermfg=7     ctermbg=0     cterm=bold
hi StatusLineNC        ctermfg=8     ctermbg=0     cterm=bold
hi VertSplit           ctermfg=0     ctermbg=0     cterm=NONE
hi TabLine             ctermfg=8     ctermbg=0     cterm=NONE
hi TabLineSel          ctermfg=7     ctermbg=0
hi Folded              ctermfg=11     ctermbg=NONE
hi Directory           ctermfg=12
hi Title               ctermfg=15     cterm=bold
hi ErrorMsg            ctermfg=15    ctermbg=1
hi DiffAdd             ctermfg=0     ctermbg=2
hi DiffChange          ctermfg=0     ctermbg=3
hi DiffDelete          ctermfg=0     ctermbg=1
hi DiffText            ctermfg=0     ctermbg=11    cterm=bold
hi User1               ctermfg=15    ctermbg=5
hi User2               ctermfg=15    ctermbg=8
hi User3               ctermfg=15    ctermbg=3
hi User4               ctermfg=15    ctermbg=0
hi User5               ctermfg=15    ctermbg=13
hi User6               ctermfg=15    ctermbg=14
hi User7               ctermfg=15    ctermbg=12
hi User8               ctermfg=15    ctermbg=11
hi User9               ctermfg=15    ctermbg=8
hi! link CursorColumn  CursorLine
hi! link SignColumn    LineNr
hi! link WildMenu      Visual
hi! link FoldColumn    SignColumn
hi! link WarningMsg    ErrorMsg
hi! link MoreMsg       Title
hi! link Question      MoreMsg
hi! link ModeMsg       MoreMsg
hi! link TabLineFill   StatusLineNC
hi! link SpecialKey    NonText

"}}}
" Generic syntax {{{
hi Delimiter       ctermfg=7
hi Comment         ctermfg=8   cterm=italic
hi Underlined      ctermfg=4   cterm=underline
hi Type            ctermfg=5   cterm=italic,bold
hi String          ctermfg=10
hi Keyword         ctermfg=2
hi Todo            ctermfg=1  ctermbg=NONE     cterm=bold,underline
hi Function        ctermfg=4   cterm=bold,italic
hi Identifier      ctermfg=7   cterm=NONE
hi Statement       ctermfg=1   cterm=bold
hi Constant        ctermfg=6
hi Number          ctermfg=12 cterm=bold
hi Boolean         ctermfg=11
hi Special         ctermfg=9
hi Ignore          ctermfg=0
hi Operator        ctermfg=9
hi Label           ctermfg=5  cterm=bold
hi Exception       ctermfg=1  cterm=bold
hi! link PreProc   Delimiter
hi! link Error     ErrorMsg
hi Conditional     ctermfg=6 cterm=italic
hi FunctionCall    ctermfg=4 cterm=italic

"}}}
" HTML {{{
hi htmlTagName              ctermfg=9
hi! link htmlSpecialTagName htmlTagName
hi! link htmlTagN htmlTagName
hi htmlTag                  ctermfg=7
hi htmlArg                  ctermfg=11
hi htmlString               ctermfg=10
hi htmlH1                   cterm=bold
hi htmlBold                 cterm=bold
hi htmlItalic               cterm=underline
hi htmlUnderline            cterm=underline
hi htmlBoldItalic           cterm=bold,italic
hi htmlBoldUnderline        cterm=bold,underline
hi htmlUnderlineItalic      cterm=underline
hi htmlBoldUnderlineItalic  cterm=bold,underline
hi! link htmlLink           Underlined
hi! link htmlEndTag         htmlTag

"}}}
" CSS {{{
hi cssAttributeSelector ctermfg=3
hi cssBraces            ctermfg=white
hi cssClassName         ctermfg=yellow
hi cssClassNameDot      ctermfg=yellow
hi cssDefinition        ctermfg=magenta
hi cssFontAttr          ctermfg=yellow
hi cssFontDescriptor    ctermfg=magenta
hi cssFunctionName      ctermfg=blue
hi cssIdentifier        ctermfg=blue
hi cssImportant         ctermfg=magenta
hi cssInclude           ctermfg=white
hi cssIncludeKeyword    ctermfg=magenta
hi cssMediaType         ctermfg=yellow
hi cssProp              ctermfg=blue
hi cssVendor            ctermfg=darkblue
hi cssPseudoClassId     ctermfg=yellow
hi cssSelectorOp        ctermfg=magenta
hi cssSelectorOp2       ctermfg=magenta
hi cssTagName           ctermfg=magenta
" }}}
" XML {{{
hi! link xmlTagName htmlTagName
hi! link xmlTag     htmlTag
hi! link xmlString  htmlString
hi! link xmlAttrib  htmlArg
hi! link xmlEndTag  xmlTagName
hi! link xmlEqual   xmlTag

"}}}
" JavaScript {{{
hi! link javaScript        Normal
hi! link javaScriptBraces  Delimiter
hi jsVariableDef  ctermfg=3
hi! link jsStorageClass Type
hi! jsFuncCall ctermfg=4
hi! link jsImport Type
hi! link jsFrom Type
hi! link jsExport Type
hi jsModuleKeyword ctermfg=9
hi jsReturn ctermfg=13
hi! link jsConditional Conditional
hi jsRepeat ctermfg=1
hi jsObjectKey ctermfg=12
hi! link jsFunctionKey FunctionCall
hi jsThis ctermfg=9 cterm=bold,italic
hi! link jsDestructuringPropertyValue jsVariableDef
"}}}
" {{{ Typescript 
hi! link typescriptBraces  javaScriptBraces
hi! link typescriptImport jsImport
hi! link typescriptExport jsExport
hi! link typescriptFuncKeyword jsFunctionKey
hi! link typescriptIdentifierName jsModuleKeyword
hi! link typescriptVariable jsStorageClass
hi! link typescriptVariableDeclaration jsVariableDef
hi! link typescriptIdentifierName jsDestructuringPropertyValue
" }}}
" Python {{{
hi! pythonPreCondit      ctermfg=13
hi! link pythonConditional Conditional
hi! pythonDocString      ctermfg=10
hi! pythonClassDef       ctermfg=13
hi! pythonClassName      ctermfg=11 cterm=bold
hi! pythonSuperClass     ctermfg=10
hi! pythonFuncDef        ctermfg=13 cterm=bold
hi! pythonBuiltinFunc    ctermfg=4
hi! link pythonNumber    Number
hi! link pythonOperator Operator
hi! link pythonPseudoOperator pythonOperator
hi! pythonDecorator      ctermfg=12 cterm=italic
hi! pythonDecoratorName      ctermfg=12 cterm=italic
hi! pythonRepeat         ctermfg=1
hi! pythonStatement ctermfg=5
hi! pythonBuiltin        ctermfg=4
hi! link pythonImport Type
hi! link pythonStrFormat String
hi! link pythonFuncCall FunctionCall
hi! pythonClass ctermfg=3 cterm=bold,italic
hi! link pythonClassVar pythonClass
hi pythonAsync ctermfg=6
hi! link pythonConstant Constant
hi! link pythonInclude Type
hi pythonParameters ctermfg=11
hi! link pythonParameterType Constant
hi! link pythonVariableType Constant
hi! link pythonReturnType Constant
hi! pythonAttribute ctermfg=12
"}}}
" PHP {{{
hi phpSpecialFunction    ctermfg=5
hi phpIdentifier         ctermfg=11
hi! link phpVarSelector  phpIdentifier
hi! link phpHereDoc      String
hi! link phpDefine       Statement

"}}}
" Markdown {{{
hi markdownH1                       ctermfg=15 cterm=bold
hi markdownH2                       ctermfg=Blue cterm=bold
hi markdownH3                       ctermfg=Cyan cterm=bold
hi markdownH4                       ctermfg=Green  cterm=bold
hi! link markdownH5                 markdownH4
hi! link markdownH6                 markdownH4
hi! link markdownHeadingDelimiter   markdownH1
hi! link markdownURLDelimiter       Delimiter
hi! link markdownCodeDelimiter      NonText
hi! link markdownLinkTextDelimiter  Delimiter
hi! link markdownLinkDelimiter      Delimiter
hi markdownLinkText                 ctermfg=4 cterm=underline
hi  markdownUrl                     ctermfg=6 cterm=italic
hi! link markdownAutomaticLink      markdownLinkText
hi! link markdownCodeBlock          String
hi! link markdownCode               String
hi markdownBold                     cterm=bold
hi markdownItalic                   cterm=italic
hi markdownListMarker               ctermfg=5 cterm=bold

"}}}
" Ruby {{{
hi! link rubyDefine                 Statement
hi! link rubyLocalVariableOrMethod  Identifier
hi! link rubyConstant               Constant
hi! link rubyInstanceVariable       Number
hi! link rubyStringDelimiter        rubyString

"}}}
" Git {{{
hi gitCommitBranch               ctermfg=3
hi gitCommitSelectedType         ctermfg=10
hi gitCommitSelectedFile         ctermfg=2
hi gitCommitUnmergedType         ctermfg=9
hi gitCommitUnmergedFile         ctermfg=1
hi! link gitCommitFile           Directory
hi! link gitCommitUntrackedFile  gitCommitUnmergedFile
hi! link gitCommitDiscardedType  gitCommitUnmergedType
hi! link gitCommitDiscardedFile  gitCommitUnmergedFile
hi! link DiffAdded DiffAdd
hi! link DiffRemoved DiffDelete
hi diffAdded cterm=bold ctermfg=2
hi diffRemoved cterm=bold ctermfg=1
hi gitCommitSummary ctermfg=7
hi! link gitCommitBlank gitCommitSummary
hi diffLine ctermfg=6 cterm=bold


hi diffFile cterm=bold ctermfg=4
" hi diffNewFile cterm=bold ctermfg=4
" hi gitcommitDiff cterm=NONE ctermfg=4
hi diffIndexLine cterm=NONE ctermfg=3

hi gitSignAdded ctermfg=2
hi gitSignRemoved ctermfg=1
hi gitSignModified ctermfg=3


"}}}
" Vim {{{
hi! link vimSetSep    Delimiter
hi! link vimContinue  Delimiter
hi! link vimHiAttrib  Constant

"}}}
" LESS {{{
hi lessVariable             ctermfg=11
hi! link lessVariableValue  Normal

"}}}
" NERDTree {{{
hi! link NERDTreeHelp      Comment
hi! link NERDTreeExecFile  String
hi NERDTreeCWD ctermfg=3 cterm=bold
"}}}
" CocExplorer {{{
hi CocExplorerFileRootName ctermfg=3 cterm=bold
" }}}
" Vimwiki {{{
hi! link VimwikiHeaderChar  markdownHeadingDelimiter
hi! link VimwikiList        markdownListMarker
hi! link VimwikiCode        markdownCode
hi! link VimwikiCodeChar    markdownCodeDelimiter

"}}}
" Help {{{
hi! link helpExample         String
hi! link helpHeadline        Title
hi! link helpSectionDelim    Comment
hi! link helpHyperTextEntry  Statement
hi! link helpHyperTextJump   Underlined
hi! link helpURL             Underlined

"}}}
" CtrlP {{{
hi! link CtrlPMatch    String
hi! link CtrlPLinePre  Comment

"}}}
" Mustache {{{
hi mustacheSection           ctermfg=14  cterm=bold
hi mustacheMarker            ctermfg=6
hi mustacheVariable          ctermfg=14
hi mustacheVariableUnescape  ctermfg=9
hi mustachePartial           ctermfg=13

"}}}
" Shell {{{
hi shDerefSimple     ctermfg=11
hi! link shDerefVar  shDerefSimple
hi shTestOpr ctermfg=4

"}}}
" Syntastic {{{
hi SyntasticWarningSign  ctermfg=3   ctermbg=NONE
hi SyntasticErrorSign    ctermfg=1   ctermbg=NONE

"}}}
" Netrw {{{
hi netrwExe       ctermfg=9
hi netrwClassify  ctermfg=8  cterm=bold

"}}}
" ALE {{{
hi ALEErrorSign ctermfg=1
hi ALEWarningSign ctermfg=3
"}}}
" JSON {{{
hi! link jsonNumber Number
hi! link jsonBoolean Boolean
hi jsonKeyword ctermfg=12
"}}}
" C++ {{{
hi! link cInclude Type
"}}}
" LaTeX {{{
hi texSection ctermfg=2 cterm=bold
hi texBeginEndName ctermfg=4 cterm=bold
hi texBeginEnd ctermfg=3
hi texMathZoneX ctermfg=10 cterm=italic
hi texMathSymbol ctermfg=2 cterm=bold,italic
"}}}
" FZF {{{
hi fzfFg ctermfg=7
hi fzfBg ctermbg=NONE
hi fzfHl ctermfg=2
hi fzfFgPlus ctermfg=15
hi fzfBgPlus ctermbg=NONE
hi fzfHlPlus ctermfg=2
hi fzfInfo ctermfg=8
hi fzfPrompt ctermfg=4
hi fzfPointer ctermfg=1
hi fzfMarker ctermfg=2
hi fzfSpinner ctermfg=4
hi fzfHeader ctermfg=6
"}}}

" COC {{{
hi CocErrorSign  ctermfg=1 cterm=bold
hi CocWarningSign  ctermfg=3 cterm=bold
hi CocInfoSign  ctermfg=6
hi CocHintSign  ctermfg=4
hi default link CocErrorVirtualText  CocErrorSign
hi default link CocWarningVirtualText CocWarningSign
hi default link CocInfoVirtualText CocInfoSign
hi default link CocHintVirtualText CocHintSign
hi default link CocErrorHighlight   CocUnderline
hi default link CocWarningHighlight   CocUnderline
hi default link CocInfoHighlight   CocUnderline
hi default link CocHintHighlight   CocUnderline
hi CocHighlightText ctermbg=8 ctermfg=3 cterm=bold
" hi CocHighlightRead ctermbg=8 ctermfg=11 cterm=bold
" hi CocHighlightWrite cterm=bold
hi CocCodeLens ctermfg=9 cterm=bold
hi default link CocErrorFloat CocErrorSign
hi default link CocWarningFloat CocWarningSign
hi default link CocInfoFloat CocInfoSign
hi default link CocHintFloat CocHintSign
hi default link CocCursorRange Search
hi default link CocHoverRange Search
" }}}

" Blamer {{{
hi Blamer ctermfg=9 cterm=bold
" }}}

" vim: fdm=marker:sw=2:sts=2:et
