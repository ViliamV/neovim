local installed, telescope = pcall(require,"telescope")
if not installed then
    return
end

local actions = require("telescope.actions")
local actions_state = require("telescope.actions.state")
local sorters = require("telescope.sorters")
local builtin = require("telescope.builtin")

telescope.setup({
  defaults = {
    prompt_prefix = " ",
    selection_caret = "👉",
    winblend = 10,
    mappings = {
      i = {
        ["<C-j>"] = actions.move_selection_next,
        -- ["<Tab>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,
        -- ["<S-Tab>"] = actions.move_selection_previous,
        ["<C-s>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-n>"] = actions.cycle_history_next,
        ["<C-p>"] = actions.cycle_history_prev,
        ["<C-a>"] = { "<c-o>0", type = "command" }, -- go to beginning of the line
        ["<C-e>"] = { "<c-o>$", type = "command" }, -- go to end of the line
        ["<esc>"] = actions.close,
      },
      n = {},
    },
    history = {
      path = "~/.cache/nvim/telescope_history.sqlite3",
      limit = 1000,
    },
  },
  file_sorter = sorters.get_fzy_sorter,
  generic_sorter = sorters.get_fzy_sorter,
  pickers = {
    buffers = {
      ignore_current_buffer = true,
      sort_lastused = true,
      sort_mru = true,
      theme = "dropdown",
      previewer = false,
      sorter = telescope.extensions["zf-native"].native_zf_scorer(),
    },
    find_files = {
      -- find_command = {"fd", "--type", "f"},
      no_ignore = true,
    },
    live_grep = {
      mappings = {
        i = {
          ["<C-f>"] = function()
            local search = actions_state.get_current_line()
            if search ~= "" then
              builtin.grep_string({ search = search, initial_mode = "insert" })
            end
          end,
        },
      },
    },
  },
})

telescope.load_extension("zf-native")
telescope.load_extension("smart_history")

local git_files_with_backup = function()
  local ok = pcall(builtin.git_files)
  if not ok then
    builtin.find_files()
  end
end

vim.keymap.set("n", "<Leader>f", git_files_with_backup, { silent = true })
vim.keymap.set("n", "<Leader>F", "<cmd>Telescope find_files<CR>", { silent = true })
vim.keymap.set("n", "<Leader>g", "<cmd>Telescope live_grep<CR>", { silent = true })
vim.keymap.set("n", "<Leader>G", "<cmd>Telescope grep_string<CR>", { silent = true })
vim.keymap.set("n", "<Leader>/", "<cmd>Telescope current_buffer_fuzzy_find<CR>", { silent = true })
vim.keymap.set("n", "<Leader>h", "<cmd>Telescope resume<CR>", { silent = true })
vim.keymap.set("n", "<Leader>d", "<cmd>Telescope diagnostics<CR>", { silent = true })
vim.keymap.set("n", "<tab>", "<cmd>Telescope buffers<CR>", { silent = true })
