local function _update_packages(arg)
  vim.api.nvim_cmd({ cmd = "source", args = { arg.file } }, { output = false })
  local installed, packer = pcall(require, "packer")
  if not installed then
    return
  end
  local name = "snapshot_" .. os.date("%Y-%m-%d-%H-%M-%S")
  -- local path = packer.config.snapshot_path
  packer.snapshot(name)
  packer.sync()
end

-- update plugins after saving plugins file
vim.api.nvim_create_autocmd("BufWritePost", {
  callback = _update_packages,
  group = vim.api.nvim_create_augroup("Packer", { clear = true }),
  pattern = { "*/lua/config/plugins.lua" },
})

-- update theme after saving theme file
vim.api.nvim_create_autocmd("BufWritePost", {
  command = "source <afile>",
  group = vim.api.nvim_create_augroup("Theme", { clear = true }),
  pattern = { "*/lua/config/theme.lua" },
})
