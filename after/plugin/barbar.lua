local installed, bufferline = pcall(require, "bufferline")
if not installed then
  return
end

bufferline.setup({
  tabpages = true,
  closable = false,
  insert_at_end = true,
  icon_separator_active = '',
  icon_separator_inactive = '',
})

vim.keymap.set("n", "<C-l>", "<cmd>BufferNext<CR>", { silent = true })
vim.keymap.set("n", "<C-j>", "<cmd>BufferPrevious<CR>", { silent = true })
-- Maybe mapped to Ctrl+Shift+w ?
vim.keymap.set("n", "<Leader>]w", "<cmd>BufferCloseAllButCurrent<CR>", { silent = true })

-- move bufferline when opening tree
local nvim_tree_api_installed, nvim_tree_api = pcall(require, "nvim-tree.api")
if not nvim_tree_api_installed then
    return
end

local bufferline_api_installed, bufferline_api = pcall(require, "bufferline.api")
if not bufferline_api_installed then
    return
end

local function get_tree_size()
  return require("nvim-tree.view").View.width
end

nvim_tree_api.events.subscribe(nvim_tree_api.events.Event.TreeOpen, function()
  bufferline_api.set_offset(get_tree_size())
end)

nvim_tree_api.events.subscribe(nvim_tree_api.events.Event.Resize, function()
  bufferline_api.set_offset(get_tree_size())
end)
--
nvim_tree_api.events.subscribe(nvim_tree_api.events.Event.TreeClose, function()
  bufferline_api.set_offset(0)
end)
