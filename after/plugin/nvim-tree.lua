local api = require("nvim-tree.api")
local mappings = {
    { key = ".", action = "toggle_dotfiles" },
    { key = { "<CR>", "o", "l", "<2-LeftMouse>" }, action = "edit" },
    { key = "h", action = "close_node" },
    { key = "s", action = "vsplit" },
    { key = "v", action = "split" },
    { key = "r", action = "refresh" },
    { key = "ma", action = "create" },
    { key = "mr", action = "rename" },
    { key = "md", action = "trash" },
    { key = "mm", action = "cut" },
    { key = "mc", action = "copy" },
    { key = "mp", action = "paste" },
    { key = "yn", action = "copy_name" },
    { key = "yP", action = "copy_absolute_path" },
    { key = "yp", action = "copy_path" },
    { key = "go", action = "open_no_close", action_cb = function(_)
        api.node.open.edit()
        api.tree.open()
    end,
    },
    { key = "q", action = "close" },
    { key = "?", action = "toggle_help" },
    { key = "p", action = "parent_node" },
    { key = "u", action = "dir_up" },
}
require("nvim-tree").setup({
    view = {
        mappings = {
            custom_only = true,
            list = mappings,
        },
    },
    filters = {
        custom = {
            "__pycache__",
            "\\.a$",
            "\\.aux$",
            "\\.blg$",
            "\\.lof$",
            "\\.lot$",
            "\\.o$",
            "\\.obj$",
            "\\.out$",
            "\\.pyc$",
            "\\.xmpi$",
        },
    },
    actions = {
        open_file = {
            quit_on_open = true,
        },
    },
    trash = {
        cmd = "trash",
    },
    renderer = {
        indent_markers = {
            enable = true,
        },
    },
    git = {
        ignore = false,
    },
})

vim.keymap.set("n", "<C-n>", api.tree.toggle, { silent = true })
vim.keymap.set("n", "<C-f>", function()
    api.tree.toggle(true)
end, { silent = true })
