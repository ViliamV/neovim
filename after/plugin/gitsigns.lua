local installed, gitsigns = pcall(require, "gitsigns")
if not installed then
  return
end

gitsigns.setup({
	signs = {
		add = { hl = "GitSignsAdd", text = "▍", numhl = "GitSignsAddNr", linehl = "GitSignsAddLn" },
		change = { hl = "GitSignsChange", text = "▍", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
		delete = { hl = "GitSignsDelete", text = "▃", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
		topdelete = { hl = "GitSignsDelete", text = "🮃", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
		changedelete = { hl = "GitSignsChange", text = "▞", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
	},
	current_line_blame_opts = {
		delay = 500,
		ignore_whitespace = false,
	},
	on_attach = function(bufnr)
		local gs = package.loaded.gitsigns

		-- Navigation
		vim.keymap.set("n", "]h", function()
			if vim.wo.diff then
				return "]h"
			end
			vim.schedule(function()
				gs.next_hunk()
			end)
			return "<Ignore>"
		end, { expr = true, buffer = bufnr })

		vim.keymap.set("n", "[h", function()
			if vim.wo.diff then
				return "[h"
			end
			vim.schedule(function()
				gs.prev_hunk()
			end)
			return "<Ignore>"
		end, { expr = true, buffer = bufnr })

		-- Actions
		vim.keymap.set("n", "<Leader>B", function()
			gs.blame_line({ full = true })
		end, { buffer = bufnr })
		vim.keymap.set("n", "<leader>b", gs.toggle_current_line_blame, { buffer = bufnr })
		-- vim.keymap.set('n', '<leader>hd', gs.diffthis, {buffer=bufnr})
		-- vim.keymap.set('n', '<leader>hD', function() gs.diffthis('~') end, {buffer=bufnr})
	end,
})
