require("mason").setup({
    ui = {
        border = "rounded"
    }
})

require("mason-lspconfig").setup({
    ensure_installed = {
        "bashls",
        "cssls",
        "eslint",
        "html",
        "tsserver",
        "sumneko_lua",
        "pyright",
        "rust_analyzer",
        "taplo",
        "tailwindcss",
        "yamlls"
    },
    automatic_installation = true
})

vim.keymap.set("n", "<Leader>m", "<cmd>Mason<CR>", { silent = true })

-- LSP have to be set up the last according to the
-- https://github.com/williamboman/mason-lspconfig.nvim#setup
require("config.lsp")
