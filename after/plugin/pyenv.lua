local CONFIGS = {
  poetry = {
    title = "Poetry",
    file = "/poetry.lock",
    command = "poetry",
    args = { "env", "info", "-p" },
  },
  pipenv = {
    title = "Pipenv",
    file = "/Pipfile",
    command = "pipenv",
    args = { "--venv" },
  },
}

local original_path = vim.env.PATH
local Job = require("plenary.job")

local activate = function(env_path, title)
  vim.env.VIRTUAL_ENV = env_path
  vim.env.PATH = env_path .. "/bin:" .. vim.env.PATH
  vim.cmd("LspRestart")
  vim.notify("Activated virtualenv", vim.log.levels.INFO, { title = title })
end

local deactivate = function()
  vim.env.VIRTUAL_ENV = nil
  vim.env.PATH = original_path
end

local _check = function(config)
  local match = vim.fn.glob(vim.fn.getcwd() .. config.file)
  if match ~= "" then
    Job:new({
      command = config.command,
      args = config.args,
      on_stdout = vim.schedule_wrap(function(_, line)
        local venv = vim.fn.trim(line)
        if (venv ~= "") and (not string.find(vim.env.PATH or "", ".venv")) then
          activate(venv, config.title)
        end
      end),
    }):start()
  end
end

local check = function()
  _check(CONFIGS.poetry)
  _check(CONFIGS.pipenv)
end

-- run on startup
vim.defer_fn(check, 500)
-- and when changing directory
vim.api.nvim_create_autocmd({ "DirChanged" }, {
  callback = function()
    deactivate()
    check()
  end,
  group = vim.api.nvim_create_augroup("pyenv", { clear = true }),
})
