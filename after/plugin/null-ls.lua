local null_ls = require("null-ls")
null_ls.setup({
    debounce = 500,
  sources = {
    -- universal
    null_ls.builtins.completion.spell,
    -- python
    null_ls.builtins.diagnostics.mypy,
    null_ls.builtins.formatting.black,
    null_ls.builtins.formatting.isort,
    null_ls.builtins.diagnostics.pylint.with({
      filter = function(diagnostic)
        if diagnostic.symbol == "wrong-import-order" then
          return false
        elseif diagnostic.symbol == "ungrouped-imports" then
          return false
        end
        return true
      end,
    }),
    -- js/ts
    -- null_ls.builtins.diagnostics.eslint,
    -- null_ls.builtins.diagnostics.xo,
    -- null_ls.builtins.formatting.prettier,
    -- lua
    null_ls.builtins.formatting.stylua,
  },
})
