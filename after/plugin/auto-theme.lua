DARK_THEME = "nightfox"
LIGHT_THEME = "dayfox"

local function set_theme(light_dark)
  local theme = light_dark == "dark" and DARK_THEME or LIGHT_THEME
  local current_theme = vim.g.colors_name
  if current_theme ~= theme then
    vim.api.nvim_cmd({
      cmd = "colorscheme",
      args = { theme },
    }, { output = false })
  end
end

set_theme(vim.env.THEME)

local function toggle_theme()
  local current_theme = vim.g.colors_name
  local theme = current_theme == DARK_THEME and LIGHT_THEME or DARK_THEME
  vim.api.nvim_cmd({
    cmd = "colorscheme",
    args = { theme },
  }, { output = false })
  local installed, lualine = pcall(require, "lualine")
  if not installed then
    return
  end
  lualine.setup({})
end

vim.api.nvim_create_autocmd("Signal", {
  pattern = "SIGUSR1",
  callback = function()
    -- added defer because otherwise lualine and barbar did not refresh correctly
    vim.defer_fn(toggle_theme, 10)
  end,
  group = vim.api.nvim_create_augroup("ThemeToggle", { clear = true }),
})
