local NORMAL = ""
local VISUAL = "﯎"
local VISUAL_LINE = "﯎ 艹"
local VISUAL_BLOCK = "﯎ "
local INSERT = "פֿ"
local REPLACE = "﯒"
local COMMAND = "גּ"
local TERMINAL = ""
local UNKNOWN = ""

local modes = {
  ["n"] = NORMAL,
  ["niI"] = NORMAL,
  ["niR"] = NORMAL,
  ["niV"] = NORMAL,
  ["nt"] = NORMAL,
  ["ntT"] = NORMAL,
  ["v"] = VISUAL,
  ["vs"] = VISUAL,
  ["V"] = VISUAL_LINE,
  ["Vs"] = VISUAL_LINE,
  ["\22"] = VISUAL_BLOCK,
  ["\22s"] = VISUAL_BLOCK,
  ["i"] = INSERT,
  ["ic"] = INSERT,
  ["ix"] = INSERT,
  ["R"] = REPLACE,
  ["Rc"] = REPLACE,
  ["Rx"] = REPLACE,
  ["Rv"] = REPLACE,
  ["Rvc"] = REPLACE,
  ["Rvx"] = REPLACE,
  ["c"] = COMMAND,
  ["cv"] = COMMAND,
  ["ce"] = COMMAND,
  ["r"] = REPLACE,
  ["rm"] = REPLACE,
  ["t"] = TERMINAL,
}

local function mode()
  local mode_code = vim.api.nvim_get_mode().mode
  if modes[mode_code] == nil then
    return UNKNOWN
  end
  return modes[mode_code]
end

local function session()
  local installed, auto_session = pcall(require, "auto-session-library")
  if not installed then
    return ''
  end
  local current = auto_session.current_session_name()
  if current == "" then
    return ''
  else
    return " "
  end
end

require("lualine").setup({
  options = {
    icons_enabled = true,
    theme = "auto",
    -- component_separators = { left = "", right = "" },
    -- section_separators = { left = "", right = "" },
    component_separators = "|",
    section_separators = { left = "", right = "" },
    disabled_filetypes = {},
    always_divide_middle = true,
  },
  sections = {
    lualine_a = { mode },
    lualine_b = { session, "branch", "diff", "diagnostics" },
    lualine_c = { "filename" },
    lualine_x = { "filetype" },
    -- lualine_x = { "encoding", "fileformat", "filetype" },
    lualine_y = {},
    -- lualine_y = { "progress" },
    lualine_z = { "location" },
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { "filename" },
    lualine_x = {},
    -- lualine_x = { "location" },
    lualine_y = {},
    lualine_z = {},
  },
  tabline = {},
  extensions = { "nvim-tree" },
})
