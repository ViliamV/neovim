vim.keymap.set("v", "<up>", "<cmd>MoveBlock(-1)<CR>", { silent = true })
vim.keymap.set("v", "<down>", "<cmd>MoveBlock(1)<CR>", { silent = true })
vim.keymap.set("v", "<left>", "<cmd>MoveHBlock(-1)<CR>", { silent = true })
vim.keymap.set("v", "<right>", "<cmd>MoveHBlock(1)<CR>", { silent = true })
